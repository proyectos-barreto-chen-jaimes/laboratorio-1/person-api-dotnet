# Person Api Dotnet
Laboratorio 1 de Arquitectura de Software, un servicio con API Rest que permite manejo de personas, profesiones, estudios y telefonos.

Implementación de Monolito con patron MVC y DAO

Stack

.NET 6
MS SQL Server 2019
REST
Swagger 3

## Diseño
----
### Arquitectura de alto nivel
![ArquitecturaAltoNivel](docs/ArquitecturaAltoNivel.png)
### Modelo de datos
![AModeloDatos](docs/ModeloDatos.png)
## Instrucciones para desplegar
----
### Requerimientos 
1. Tener instalado SQL Server 2019 Express modo Básico.
2. Tener instalado SQL Server Management Studio 19.
3. Tener instalado visual estudio comunity 2022.
---
### Pasos
---
1. Clonar el repositorio.
'''
    git clone https://gitlab.com/proyectos-barreto-chen-jaimes/laboratorio-1/person-api-dotnet.git
'''
2. Abrir SQL Server Management Studio
3. En connect to server:
    a. Server Type: Database Engine
    b. Server name: localhost\SQLEXPRESS
    c. Authentication: Windows Authentication
4. Abrir el script DDL llamado scripts/SQLInitialDB.ddl.sql
5. Ejecutarlo desde SQL Server Management Studio con execute query con lo que quedaran creadas las tablas del modelo.
6. Abrir el script DML llamado scripts/SQLInitialDB.dml.sql
7. Ejecutarlo desde SQL Server Management Studio con execute query con lo que ya tendras datos de pruebas.
8. Abrir el proyecto personapi-dotnet.sln que esta en la raiz del repositorio en visual studio.
9. En SQL Server Object Explorer poner Add SQL Server y poner
    a. Server Name: localhost\SQLEXPRESS
    b. lo demás se deja igual.
10. Finalmente dar start en el símbolo del play para desplegar el proyecto.
11. Se abrira el navegado en la ruta http://localhost:5028/
12. Se tendrá el menú.
    a. Personas.
    b. Telefonos.
    c. Profesiones.
    d. Estudios.
## Funcionamiento
----
![AModeloDatos](docs/Funcionamiento.png)
## Author
----
Juan Sebastian Barreto Jimenéz - [jsebastianbarretoj99](https://gitlab.com/jsebastianbarretoj99)

Janet Chen He - [XingYi98](https://gitlab.com/XingYi98)

Noah Daniela Jaime - [XingYi98](https://gitlab.com/noahdaniela)
