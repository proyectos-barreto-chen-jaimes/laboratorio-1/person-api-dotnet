IF NOT EXISTS (SELECT [name] FROM [sys].[databases] WHERE [name] = 'persona_db')
BEGIN
	CREATE DATABASE [persona_db];
	PRINT 'Se ha creado la base de datos persona_db.'
END
ELSE
BEGIN
	PRINT 'La base de datos persona_db ya existe.'
END
G

USE [persona_db]
GO

IF NOT EXISTS (SELECT [name] FROM sys.tables WHERE [name] = 'persona')
BEGIN
	-- Table `persona_db`.`persona`
	CREATE TABLE [persona] (
	[cc] INT NOT NULL,
	[nombre] VARCHAR(45) NULL,
	[apellido] VARCHAR(45) NULL,
	[genero] CHAR(1) NULL CHECK ([genero] IN ('M', 'F')),
	[edad] INT NULL,
	CONSTRAINT [pk_persona] PRIMARY KEY ([cc])
	);
	PRINT 'Se ha creado la tabla persona en persona_db.'
END
ELSE
BEGIN
	PRINT 'La tabla persona ya existe en persona_db.'
END
GO

IF NOT EXISTS (SELECT [name] FROM sys.tables WHERE [name] = 'profesion')
BEGIN
  -- Table `arq_per_db`.`profesion`
  CREATE TABLE [profesion] (
    [id] INT NOT NULL,
    [nom] VARCHAR(90) NULL,
    [des] NVARCHAR(MAX) NULL,
    CONSTRAINT [pk_profesion] PRIMARY KEY ([id])
  );
  PRINT 'Se ha creado la tabla profesion en persona_db.'
END
ELSE
BEGIN
	PRINT 'La tabla profesion ya existe en persona_db.'
END
GO

IF NOT EXISTS (SELECT [name] FROM sys.tables WHERE [name] = 'estudios')
BEGIN
  -- Table `arq_per_db`.`estudios`
  CREATE TABLE [estudios] (
    [id_prof] INT NOT NULL,
    [cc_per] INT NOT NULL,
    [fecha] DATE NULL DEFAULT NULL,
    [univer] VARCHAR(50) NULL DEFAULT NULL,
    CONSTRAINT [pk_estudios] PRIMARY KEY ([id_prof], [cc_per]),
    CONSTRAINT [fk_estudios_persona] FOREIGN KEY ([cc_per]) REFERENCES [persona] ([cc]),
    CONSTRAINT [fk_estudios_profesion] FOREIGN KEY ([id_prof]) REFERENCES [profesion] ([id])
  );
  PRINT 'Se ha creado la tabla estudios en persona_db.'
END
ELSE
BEGIN
	PRINT 'La tabla estudios ya existe en persona_db.'
END
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'estudio_persona_fk' AND object_id = OBJECT_ID(N'[dbo].[estudios]'))
BEGIN
    CREATE NONCLUSTERED INDEX [estudio_persona_fk] ON [dbo].[estudios]
    (
        [cc_per] ASC
    )
    INCLUDE (fecha, univer)
  PRINT 'Se ha creado el indice en estudios en persona_db.'
END
ELSE
BEGIN
	PRINT 'El indice ya existe en estudios en persona_db.'
END
GO

IF NOT EXISTS (SELECT [name] FROM sys.tables WHERE [name] = 'telefono')
BEGIN
  -- Table `arq_per_db`.`telefono`
  CREATE TABLE [telefono] (
    [num] VARCHAR(15) NOT NULL,
    [oper] VARCHAR(45) NOT NULL,
    [duenio] INT NOT NULL,
    CONSTRAINT [pk_telefono] PRIMARY KEY ([num]),
    CONSTRAINT [fk_telefono_persona] FOREIGN KEY ([duenio]) REFERENCES [persona] ([cc])
  );
  PRINT 'Se ha creado la tabla telefono en persona_db.'
END
ELSE
BEGIN
	PRINT 'La tabla telefono ya existe en persona_db.'
END
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'telefono_persona_fk' AND object_id = OBJECT_ID(N'[dbo].[telefono]'))
BEGIN
    CREATE NONCLUSTERED INDEX [telefono_persona_fk] ON [dbo].[telefono]
    (
        [duenio] ASC
    )
    INCLUDE (oper)
  PRINT 'Se ha creado el indice en telefono en persona_db.'
END
ELSE
BEGIN
	PRINT 'El indice ya existe en telefono en persona_db.'
END
GO